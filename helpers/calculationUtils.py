import numpy as np


def normalise_values(values):
    return values / np.amax(values)


def get_indexes_of_data_points(scale, x_min, x_max):
    return [index for index, value in enumerate(scale) if (value >= x_min) and (value <= x_max)]
