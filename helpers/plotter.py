import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def plot_surface(x, y, value, title):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.set_xlabel("x[mm]")
    ax.set_ylabel("y[mm]")
    ax.set_zlabel("Relative Dose")

    surf = ax.plot_trisurf(x, y, value, cmap=cm.coolwarm)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title(title)
    plt.show()
