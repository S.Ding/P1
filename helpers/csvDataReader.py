import csv


def read(file_path):
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        measurement_data = {
            "x": [],
            "y": [],
            "z": [],
            "value": []
        }
        for row in csv_reader:
            is_comment = False
            for entry in row:
                if '#' in entry:
                    is_comment = True
            if is_comment:
                continue

            measurement_data["x"].append(float(row[0]))
            measurement_data["y"].append(float(row[1]))
            measurement_data["z"].append(float(row[2]))
            measurement_data["value"].append(float(row[3]))

        return measurement_data
