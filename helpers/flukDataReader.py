import csv


def read(file_path):
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        value = []
        for row in csv_reader:
            is_comment = False
            for entry in row:
                if '#' in entry:
                    is_comment = True
            if is_comment:
                continue

            s = [7, 9, 11, 13, 15, 17, 19, 21, 23, 25]
            for i in s:
                value.append(float(row[i]))

        return value
