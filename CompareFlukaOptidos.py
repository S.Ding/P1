import helpers.flukDataReader as reader
import helpers.csvDataReader as csvReader
import helpers.calculationUtils as calUtils
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from collections import namedtuple

simulation_data = np.array(reader.read("data/Fluka/praktikum001_fort.22.lis"))
optidos_long_scan_data = csvReader.read('data/Optidos/long_scan_saved_GL.txt')

PlotSetting = namedtuple('PlotSetting', 'z offset fit')

number_of_bins_along_x = 180
number_bins_along_y_and_z = 320
x_scale = np.linspace(-1, 8, number_of_bins_along_x)
y_and_z_scale = np.linspace(-4, 4, number_bins_along_y_and_z)

y_values = optidos_long_scan_data["y"][4::9]
dose_value = optidos_long_scan_data["value"][4::9]

plot_settings = [PlotSetting(0, 38, 0), PlotSetting(1, 58, 0), PlotSetting(2, 78, 0.3), PlotSetting(3, 98, 0.7),
                 PlotSetting(4, 118, 1.9), PlotSetting(5, 138, 3.5)]

for setting in plot_settings:
    data_along_y = simulation_data[
                   number_of_bins_along_x * number_bins_along_y_and_z * 159 + setting.offset::number_of_bins_along_x]
    normalised_data_along_y = calUtils.normalise_values(data_along_y[0:number_bins_along_y_and_z])

    if setting.fit == 0:
        plt.plot(y_and_z_scale, normalised_data_along_y, label="simulation")
    else:
        fit = UnivariateSpline(y_and_z_scale, normalised_data_along_y, s=setting.fit)
        normalisedFitData = calUtils.normalise_values(fit(y_and_z_scale))
        plt.plot(y_and_z_scale, normalisedFitData, label="fit of simulation")

    normalised_dose_values = calUtils.normalise_values(dose_value[setting.z * 9:(setting.z + 1) * 9])
    plt.plot(np.array(y_values[setting.z * 9:(setting.z + 1) * 9]) - 26.3, normalised_dose_values, label="measurement")
    plt.title("Comparison of relative dose distributions along y axis at z={z}mm".format(z=setting.z))
    plt.ylabel("Relative Dose")
    plt.xlabel("y[mm]")
    plt.legend()
    plt.grid(True)
    plt.show()
