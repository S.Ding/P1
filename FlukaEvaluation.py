import helpers.flukDataReader as reader
import helpers.calculationUtils as calUtils
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from collections import namedtuple

simulation_data = np.array(reader.read("data/Fluka/praktikum001_fort.22.lis"))
PlotSetting = namedtuple('PlotSetting', 'z offset fit')

number_of_bins_along_x = 180
number_of_bins_along_y_and_z = 320
x_scale = np.linspace(-1, 8, number_of_bins_along_x)
y_and_z_scale = np.linspace(-4, 4, number_of_bins_along_y_and_z)

# Plot Fluka simulation data along y-axis for different z values
plot_settings = [PlotSetting(0, 38, 0), PlotSetting(1, 58, 0), PlotSetting(2, 78, 0.3), PlotSetting(3, 98, 0.7),
                 PlotSetting(4, 118, 1.9), PlotSetting(5, 138, 3.5)]

for setting in plot_settings:
    data_along_y = simulation_data[
                 number_of_bins_along_x * number_of_bins_along_y_and_z * 159 + setting.offset::number_of_bins_along_x]

    normalised_data_along_y = calUtils.normalise_values(data_along_y[0:number_of_bins_along_y_and_z])
    plt.plot(y_and_z_scale, normalised_data_along_y, label="{z}mm".format(z=setting.z))

    if setting.fit > 0:
        fit = UnivariateSpline(y_and_z_scale, normalised_data_along_y, s=setting.fit)
        plt.plot(y_and_z_scale, fit(y_and_z_scale), label="fit")

    plt.title("Relative Dose along y axis through source center at z={z}mm".format(z=setting.z))
    plt.ylabel("Relative Dose")
    plt.xlabel("y[mm]")
    plt.legend()
    plt.grid(True)
    plt.show()

# Plot Fluka simulation data along x-axis for z=0
data_along_x = simulation_data[number_of_bins_along_x * 159 + 38::number_of_bins_along_x * number_of_bins_along_y_and_z]
normalised_data_along_x = calUtils.normalise_values(data_along_x[0:number_of_bins_along_y_and_z])
plt.title("Relative Dose along x axis through source center at z=0mm")
plt.ylabel("Relative Dose")
plt.xlabel("x[mm]")
plt.plot(y_and_z_scale, normalised_data_along_x)
plt.show()
