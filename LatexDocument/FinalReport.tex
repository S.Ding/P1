\documentclass[12pt]{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{setspace}
\usepackage{geometry}
\usepackage{fancybox}
\usepackage{color}
\usepackage{gensymb}
\usepackage{wrapfig}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{caption}
\geometry{a4paper, top=20mm, left=20mm, right=15mm, bottom=20mm}


\begin{document}
	
\title{\vspace{-3.0cm}\large P1 -- Dosimetry}
\author{Samuel Lerch, Simon Ding}
\date{\small \today}
\maketitle

\onehalfspacing

\section{Aim of the experiment}


The aim of the experiment is to investigate a special dosimetry method. The method consists of measurements with a scintillaion detector in a water phantom.
A Strontium-90 radiation source is used to generate a typical dose distrubution. Such beta-emitting sources are usually employed in brachytherapy.
The results of the measurement are compared with a Monte-Carlo simulation which predicts the dose distribution in the water phantom.

\section{Foundations}


\subsection{Radiation source}
Strontium is an earth alkaline metal with a atomic number of 38. Strontium-90 is a radiating isotope of Strontium that is beta-emitting with a half-life of 28.7 years. As shown in Figure 1 the only relevant decay type for Strontium-90 is beta decay.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{"Sr90_decay"}
	\caption[Sr-90 decay chain]{Sr-90 decay chain}
	\label{fig:sr90decay}
\end{figure}

\subsection{Scintillation detector and dosimeter}
In the experiment we use the Optidos scintillation system. It consists of a scintillation-detector (Type 60006) and a scintillation-dosimeter (Optidos Type 10013).
The scintillation-detector contains a cylindrical standard organic scintillator with a diameter of 1mm and a height of 1mm. The active detection volume amounts to 0.8mm. It sits 0.4mm behind the front edge of the detector assembly.
The dosimeter contains a photomultiplier which converts light entering the dosimeter through the glass fiber. Therefore the Optidos device doesn't count single events, but measures the stream of photons. The maximum accuracy of 0.5\% can be provided between a dose rate from 0.4 mGy/s to 0.4 Gy/s.

\section{Experimental procedure}


\subsection{preparing the experiment}
First the Dosimeter with the scintillation-detector is calibrated. After that the detector is fixed into a holder which is attached with a motor.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{"Detector"}
	\caption[Scintillation detector]{Scintillation detector}
	\label{fig:detector}
\end{figure}

\subsection{Measurement of the depth dose distribution}
In a first step the radiation is measured in directions vertical to the source (We will define this as x- and y-direction) with a resolution of 0.1 mm. From that we can obtain the center of the source, which is located at $x=22.3$mm $y=26.3$mm. Then the depth dependence (defined as dose distribution in z-direction) of the radiation at the central axis is measured from $z=0$mm to $z=6$mm. This is also done with a resolution of 0.1 mm. As a result we get a graph of the radiation dependent of the depth. (Figure \ref{fig:depthdose2})

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{../DingLerch/Optidos/depthdose2}
	\caption{Depth dose distribution}
	\label{fig:depthdose2}
\end{figure}

\subsection{Grid scan}
Finally we do a 3D scan of the most interesting area (in a 4mm by 4mm square with the source as the center with a resolution of 0.5mm and 5mm in z-direction with a resolution of 1mm). From that we get six 2D plots (in x-and y-direction) at different depths.

\newpage
\section{Data evaluation}

This section will first discuss the characteristics of the data, obtained from the Optidos system and the Monte-Carlo simulation, followed by a comparison of these two results. Finally we discuss possible reasons for their differences.

\noindent For the comparison it is important to note that the data obtained from the simulation is not in SI units but in $\frac{GeV}{g}$. In addition the output is normed per particle. For a conversion a multiplication with the factor of $1.602\cdot10^{-7}$ and the activity of the source would be required. In the following we will compare the relative distribution of the dose, so a explicit conversion will not be necessary anymore.

\subsection{Optidos data}
The following figures will show the dose distribution measured in each plane at different depths, with all values normalised in respect of the maximum dose.

\begin{minipage}{0.5\textwidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_0}
	\captionof{figure}{Dose Distrubution at 0mm}
	\label{fig:planeplot0}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_1}
	\captionof{figure}{Dose Distrubution at 1mm}
	\label{fig:planeplot1}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_2}
	\captionof{figure}{Dose Distrubution at 2mm}
	\label{fig:planeplot2}
\end{minipage}
\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_3}
	\captionof{figure}{Dose Distrubution at 3mm}
	\label{fig:planeplot3}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_4}
	\captionof{figure}{Dose Distrubution at 4mm}
	\label{fig:planeplot4}
	\centering
	\includegraphics[width=\textwidth]{Bilder/plane_plot_5}
	\captionof{figure}{Dose Distrubution at 5mm}
	\label{fig:planeplot5}
\end{minipage}

\newpage
\noindent At a first glance the shape of the surface plots seem to be exactly the same. But after looking at the scale included on the right side of each figure, one can see that the range of the scale decreases with increasing depth. That means that the peaks are getting flatter. This can be visualised by plotting the dose distribution of the plane along one axis as shown in figure \ref{fig:comparisonoptidoseyaxis}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Bilder/comparison_optidose_y_axis}
	\caption{Dose distribution along y axis}
	\label{fig:comparisonoptidoseyaxis}
\end{figure}

\subsection{FLUKA data}
The Monte-Carlo simulation was implemented using FLUKA. All values concerning the setup, such as meterials and geometry was preset for this experiment. The only thing required was the number of events (in the context of this experiment the number of Strontium atom decays) and the resolution. The resolution was set using a parameter that determined the number of voxels that would each measure the deposited energy in a defined area. For the depth dose distribution in the simulation, we used a binning size of 0.025mm from $z=-1$mm to $z=8$mm. For the grid scan in our simulation we used voxels with the dimension of 0.025mm length along the x- and y-axis from -4mm to 4mm and 0.05mm length along the z-axis from -1mm to 8mm. The figures below visualises the results from different perspectives.

\begin{minipage}{0.5\textwidth}
	\centering
	\includegraphics[width=0.8\linewidth]{../DingLerch/xyDistGeometry}
	\captionof{figure}{z-y-View}
	\label{fig:zydistgeometry}
	
	\centering
	\includegraphics[width=0.8\linewidth]{../DingLerch/yzDistribution}
	\captionof{figure}{x-y-View}
	\label{fig:xydistgeometry}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\centering
	\includegraphics[width=0.8\linewidth]{../DingLerch/xzDistribution}
	\captionof{figure}{x-z-View}
	\label{fig:xzdistgeometry}
	
	\centering
	\includegraphics[width=0.8\linewidth]{../DingLerch/DepthDoseDistribution}
	\captionof{figure}{Depth dose distribution}
	\label{fig:depthdosefluka}
	
\end{minipage}

\noindent \phantom{adf} \newline
\phantom{bla} \newline
Immediatly it is strinking that the x-y-View (\ref{fig:xydistgeometry}) shows a asymetric dose distribution. Assuming that our source is a cylinder with radius of 0.15mm, the x-y-View should depict a peak dose area in form of a circle. Instead we have a ellipse shaped form. After talking with our instructor and reviewing the simulation setup, we came to the conclusion that there was a non trivial error that occured during the simulation. So for the following comparison of the distribution in the planes, we will only consider the distribution along the y-axis which seem to be the expected distribution.

\newpage
\subsection{Comparison of depth scan}

Plotting each dose distribution along the z-axis with values relative to their respective maximum will yield the two graphs shown in picture \ref{fig:normaliseddepthdoses} below.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Bilder/normalised_depth_doses}
	\caption{Relative dose distributions}
	\label{fig:normaliseddepthdoses}
\end{figure}
It is striking that the measured dose with the Opitidos system is falling off exponentially immediately, while the simulated dose peaks after 0mm, before falling off. This can be explained by a shift of the z-axis, since the Optidos sensor at $z=0mm$ is still a little bit away from the source and also for the simulation $z=0mm$ means the center of the source, which cannot be measured by the optidos detector. The find this offset of the two axis we defined an algorithm to shift the data of the simulation to the left by a small amount and then calculate the difference between the two graphs and determine at which offset this difference will be the smallest. It is important to renormalised the simualation data after the shift, since we will not consider values at a depth smaller than zero. Therefore we have to calculate the relative dose based off a new peak value. With this algorithm we determined the shift to be at 0.93mm. Below we illustrated some iterations of the algorithm along with the plot for the correct shift.
For the following parts $z=0$mm will refer to the starting point of the optidos system (like in Figure \ref{fig:normaliseddepthdoses}).

\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/depth_doses_with_shift_0}
	\captionof{figure}{Depth doses without shift}
	\label{fig:depthdoseshift0}
	\centering
	\includegraphics[width=\textwidth]{Bilder/depth_doses_with_shift_08}
	\captionof{figure}{Depth doses with 0.8mm shift}
	\label{fig:depthdoseshift08}
\end{minipage}
\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/depth_doses_with_shift_04}
	\captionof{figure}{Depth doses with 0.4mm shift}
	\label{fig:depthdoseshift04}
	\centering
	\includegraphics[width=\textwidth]{Bilder/depth_doses_with_shift_12}
	\captionof{figure}{Depth doses with 1.2mm shift}
	\label{fig:depthdoseshift12}
\end{minipage}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.65\textwidth]{Bilder/depth_doses_with_correct_shift}
	\caption{Depth doses with correct shift of 0.93mm}
	\label{fig:depthdoseshiftcorrect}
\end{figure}

\subsection{Comparison of dose distribution along y-axis}

Now that we have the shift along the z-axis, we will first have a look at the simulated data for the distribution along the y-axis (that go through the center of the source) at same depths as the optidos (Shown in figures \ref{fig:flukay0} to \ref{fig:flukay5}). 

\noindent With increasing depth more noise is introduced into the graphs. This is because the voxels further from the source are registering less energy deposited (that means less radiation or decay events gets detected), so the statistical fluctuation is getting more significant. To help better visualise the form of the graphs, we included a function fit (orange graph) starting at depth $z=2$mm. For a consistency check to verify the distribution depicted in figure \ref{fig:xydistgeometry}, we also plotted a dose distribution along the x axis at $z=0$mm (See figure \ref{fig:flukax0}). As you can see the width of the peak is much smaller than the one for the y axis distribution (Figure \ref{fig:flukay0}).

\begin{center}
	\includegraphics[width=0.7\linewidth]{Bilder/fluka_x_at_0}
	\captionof{figure}{FLUKA Dose distribution along x axis at z=0mm}
	\label{fig:flukax0}
\end{center}

Comparing the dose distribution of the planes with the optidos data we get figures \ref{fig:compare0} to \ref{fig:compare5}. In figure \ref{fig:compare0} and figure \ref{fig:compare1} we can see that the graph from the simulation falls off a lot steeper than the graph from the optidos measurement. While in figure \ref{fig:compare2}, \ref{fig:compare3} and \ref{fig:compare4} the graphs almost matches perfectly. For figure \ref{fig:compare5} the graph of the optidos measurement falls off slightly quicker than the simulation data. The stark difference in figure \ref{fig:compare0} and \ref{fig:compare1} could be explained by the different resolutions of the system. The simulation records the distrubtion with a much higher resolution (320 data points), while the optidos system only measure nine positions along the y axis. Furthermore the size of the scintillation detector alone is larger than the voxels used in the simulation. Therefore the simulation can detect a decline of the radiation much better than the optidos system.

\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_0}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=0mm}
	\label{fig:flukay0}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_2}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=2mm}
	\label{fig:flukay2}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_4}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=4mm}
	\label{fig:flukay4}
\end{minipage}
\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_1}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=1mm}
	\label{fig:flukay1}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_3}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=3mm}
	\label{fig:flukay3}
	\centering
	\includegraphics[width=\textwidth]{Bilder/fluka_y_at_5}
	\captionof{figure}{FLUKA Dose distribution along y axis at z=5mm}
	\label{fig:flukay5}
\end{minipage}

\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_0}
	\captionof{figure}{Comparison of dose distributions along y axis at z=0mm}
	\label{fig:compare0}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_2}
	\captionof{figure}{Comparison of dose distributions along y axis at z=2mm}
	\label{fig:compare2}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_4}
	\captionof{figure}{Comparison of dose distributions along y axis at z=4mm}
	\label{fig:compare4}
\end{minipage}
\begin{minipage}{0.5\linewidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_1}
	\captionof{figure}{Comparison of dose distributions along y axis at z=1mm}
	\label{fig:compare1}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_3}
	\captionof{figure}{Comparison of dose distributions along y axis at z=3mm}
	\label{fig:compare3}
	\centering
	\includegraphics[width=\textwidth]{Bilder/compare_plane_5}
	\captionof{figure}{Comparison of dose distributions along y axis at z=5mm}
	\label{fig:compare5}
\end{minipage}

\section{Conclusion}
Overall the data from the actual measurement with the optidos system and from the Monte-Carlo simulation do match each other in general after some adjustments. Although there is still a big error in the setting of the simulation that results in a wrong dose distribution along the x axis. Also one should keep in mind that the shift along the z axis was determined by an iterative process that just minimized the absolute difference between the two depth dose distributions. Therefore other shifts would also be possible, where the form of the distribution would be more similar, but have a slightly larger absolute difference. In addition the it is important to keep in mind that the simulation always include statistical errors, especially at locations with less absolute dosage. 

\appendix
\section{Appendix}

\paragraph{Calculating $k_p$:} The last calibration took place on October 15th, 2015. On that day $k_{p,0}$ was determined to be $1171.0\frac{mGy}{min}$. To the day of the experiment (21st June 2018) $2.6886$ years have passed. We also know that the half-life of Strontium 90 is 28.79 years. Since $k_p$ is the dose-rate we can just use the law of radioactive decay to calculate it.

\noindent Instead of $N(t)=N_0\cdot e^{-\lambda t}$, we can use $k_p=k_{p,0}\cdot e^{-\lambda t}$
\begin{align*}
k_p &= k_{p,0}\cdot e^{-\lambda t} \qquad \text{mit } \lambda=\frac{\ln2}{T_{\frac{1}{2}}} \\
	&= k_{p,0}\cdot e^{-\frac{\ln2}{T_{\frac{1}{2}}}\cdot t} \qquad \text{mit } t=2.6886year \text{ und } T_{\frac{1}{2}}=28.79year\\
	&= 0.93732 \cdot k_{p,0}\\
	&= 1097,6 \frac{mGy}{min}
\end{align*}

\noindent This result is also consistent with the prompt of the optidos system, after finishing the calibration.
\end{document}