import helpers.csvDataReader as csvReader
import helpers.flukDataReader as flukReader
import helpers.calculationUtils as calUtils
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import UnivariateSpline

optidos_depth_dose_data = csvReader.read('data/Optidos/depthdose2.txt')
normalised_optidos_values = calUtils.normalise_values(optidos_depth_dose_data["value"])

fluka_depth_dose_values = flukReader.read('data/Fluka/praktikum001_fort.21.lis')
fluka_depth_dose_values = np.array(fluka_depth_dose_values)
fluka_depth_scale = np.linspace(-1, 8, 360)
normalised_fluka_values = calUtils.normalise_values(fluka_depth_dose_values)

plt.plot(optidos_depth_dose_data["z"], normalised_optidos_values, label="measurement")
plt.plot(fluka_depth_scale, normalised_fluka_values, label="simulation")

plt.title("Relative depth dose values")
plt.ylabel('Relative Dose')
plt.xlabel('Depth[mm]')
plt.legend()
plt.grid(True)
plt.show()

all_diffs = []
offsets = np.linspace(0, 2, 201)

fit_of_optidos = UnivariateSpline(optidos_depth_dose_data["z"], normalised_optidos_values, s=0)

for i in offsets:
    shifted_scale = fluka_depth_scale - i

    index_of_selected_datapoints = calUtils.get_indexes_of_data_points(shifted_scale, 0, 6)

    selected_scale = shifted_scale[index_of_selected_datapoints]
    selected_values = calUtils.normalise_values(fluka_depth_dose_values[index_of_selected_datapoints])

    fit_of_fluka = UnivariateSpline(selected_scale, selected_values, s=0)
    fitValuesFluk = fit_of_fluka(shifted_scale[index_of_selected_datapoints])

    # Difference of Both graphs

    scale = np.linspace(0, 6, 240)

    difference = fit_of_fluka(scale) - fit_of_optidos(scale)

    absolute_diff = sum(difference ** 2)

    all_diffs.append(absolute_diff)

minDiff = np.amin(all_diffs)
indexOfMin = all_diffs.index(minDiff)

print("Offset with the smalles difference is: ", offsets[indexOfMin])

shifted_scale = fluka_depth_scale - offsets[indexOfMin]
selection_index = calUtils.get_indexes_of_data_points(shifted_scale, 0, 6)
selected_scale = shifted_scale[selection_index]
selected_values = calUtils.normalise_values(fluka_depth_dose_values[selection_index])

plt.plot(optidos_depth_dose_data["z"], normalised_optidos_values, label="measurement")
plt.plot(selected_scale, selected_values, label="simulation")
plt.title("Relative depth dose values with correct shift")
plt.ylabel("Relative Dose")
plt.xlabel("Depth[mm]")
plt.legend()
plt.grid(True)
plt.show()
