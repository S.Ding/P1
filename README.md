#Lab course P1

## Report
The final report of this lab course can be found in the folder **LatexDocument**.

## Scripts

We used four scripts for evaluating the data from the experiment.

### OptidosEvaluation.py
Visualizes the data obtained from real measurements.

### FlukaEvaluation.py
Visualizes the data obtained with the simulation using FLUKA.

### DepthDoseEvaluation.py
Compares and visualizes the data along the z-axis obtained from both setups. This script was also used to determine the z-axis offset between the actual experiment setup and the simulation.

### CompareFlukaOptidos.py
Compares the visualizes the data along the y-axis obtained from both setups.
