import helpers.csvDataReader as csvReader
import helpers.calculationUtils as calUtils
import helpers.plotter as plotter
import matplotlib.pyplot as plt

optidos_long_scan_data = csvReader.read('data/Optidos/long_scan_saved_GL.txt')

for z in range(6):
    normalisedLongScanValues = calUtils.normalise_values(optidos_long_scan_data["value"][z * 81:(z + 1) * 81])
    plotter.plot_surface(optidos_long_scan_data["x"][z * 81:(z + 1) * 81],
                         optidos_long_scan_data["y"][z * 81:(z + 1) * 81], normalisedLongScanValues,
                         "Surface plot for depth z={z}mm".format(z=z))

# 2D Plots

y_values = optidos_long_scan_data["y"][4::9]
dose_value = optidos_long_scan_data["value"][4::9]

for z in range(6):
    normalisedDoseValues = calUtils.normalise_values(dose_value[z * 9:(z + 1) * 9])
    plt.plot(y_values[z * 9:(z + 1) * 9], normalisedDoseValues, label="{z}mm".format(z=z))

plt.title("Relative Dose along y axis through source center (x=22.3mm)")
plt.xlabel("y[mm]")
plt.ylabel("relative Dose")
plt.grid(True)
plt.legend()
plt.show()
